package main

import (
	"strings"
)

// CoffeeVisitor will visit the mathl ast and generate coffeescript code
// that builds the coffeescript code to render given formula
type CoffeeVisitor struct {
	identifier int
	updaters   string
	source     string
	// node is a stack of the current node context
	node   []string
	indent int
}

func (v *CoffeeVisitor) parse(n Node, name string) error {
	cmt := v.indentString() + "#  generated " + name + "\n"
	cmt += v.indentString() + name + ": (_s, _r)->\n"
	v.indent++
	res, err := n.Accept(v)
	v.indent--
	v.source += cmt + res
	return err
}

func (v *CoffeeVisitor) pushID() string {
	v.identifier++
	v.node = append(v.node, asIdentifier(v.identifier))
	return v.peekID(0)
}

func (v *CoffeeVisitor) peekID(top int) string {
	return v.node[len(v.node)-top-1]
}

func (v *CoffeeVisitor) popID() string {
	id := v.peekID(0)
	v.node = v.node[0 : len(v.node)-1]
	return id
}

func (v *CoffeeVisitor) indentString() string {
	return strings.Repeat("    ", v.indent)
}

func (v *CoffeeVisitor) List(l *List) (string, error) {
	dec := v.indentString() + v.pushID() + " = " + v.peekID(1) + ".row()"
	if l.style != "" {
		dec += ".color(COL." + strings.ToUpper(l.style) + ")"
	}
	statements := []string{
		dec + "\n",
	}
	for _, n := range l.elements {
		r, err := n.Accept(v)
		if err != nil {
			return "", err
		}
		statements = append(statements, r)
	}
	v.popID()
	return strings.Join(statements, ""), nil
}

func (v *CoffeeVisitor) Matrix(m *Matrix) (string, error) {
	l1 := v.indentString() + v.pushID() + " = " + v.peekID(1) + ".brackets().row()\n"

	for c := 0; c < m.Width(); c++ {
		if c > 0 {
			// add a spacer column between displayed columns
			l1 += v.indentString() + v.peekID(0) + ".column().center(0.7).text('..').color '#fff0'\n"
		}
		l1 += v.indentString() + v.pushID() + " = " + v.peekID(1) + ".column().center(0.7)\n"
		for _, row := range m.elements {
			if c < len(row) {
				res, err := row[c].Accept(v)
				if err != nil {
					return "", err
				}
				l1 += res
			}
		}
		v.popID()
	}

	v.popID()
	return l1, nil
}

func (v *CoffeeVisitor) Source(s *Source) (string, error) {
	dec := v.indentString() + "window.mathl._m." + v.pushID() + " = " + v.peekID(1) + ".text('')\n"
	v.updaters += "        m." + v.peekID(0) + ".text = " + s.source + "\n"
	v.popID()
	return dec, nil
}

func (v *CoffeeVisitor) Text(t *Text) (string, error) {
	dec := v.indentString() + v.peekID(0) + ".text(" + asJsStr(t.text) + ")\n"
	return dec, nil
}
