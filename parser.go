package main

import (
	"fmt"
	"strings"
)

var (
	decorators = map[string]func(string) string{
		"hat": func(i string) string {
			return i + "\u0302"
		},
		"vec": func(i string) string {
			return i + "\u20D7"
		},
	}

	styles = map[string]string{
		"wht":   "wht",
		"white": "wht",
		"red":   "red",
		"blu":   "blu",
		"blue":  "blu",
		"grn":   "grn",
		"yel":   "yel",
		"rng":   "rng",
		"prp":   "prp",
		"gry":   "gry",
	}
)

func init() {
	s := map[string]string{}
	for k, v := range styles {
		s[k] = v
		s[k+"_d"] = v + "_d"
		s[k+"_b"] = v + "_b"
	}
	styles = s
}

func parse(c string) (*List, error) {
	parts, err := extractBrackets(c)
	if err != nil {
		return nil, err
	}

	nodes := []Node{}
	for _, part := range parts {
		if part[0] == '{' {
			n, err := processStatement(part)
			if err != nil {
				return nil, err
			}
			nodes = append(nodes, n)
		} else if part[0] == '[' {
			if len(part) < 4 {
				return nil, fmt.Errorf("minimal matrix must at least be [[]]")
			}
			if part[1] != '[' {
				return nil, fmt.Errorf("matrix must start with [[")
			}
			if part[len(part)-2] != ']' {
				return nil, fmt.Errorf("matrix must end with ]]")
			}
			n, err := processMatrix(part)
			if err != nil {
				return nil, err
			}
			nodes = append(nodes, n)
		} else {
			n, err := processText(part)
			if err != nil {
				return nil, err
			}
			nodes = append(nodes, n)
		}
	}
	return &List{
		elements: nodes,
	}, nil
}

func extractBrackets(c string) ([]string, error) {
	parts := []string{}
	start := 0
	s, e := 0, 0
	var err error
	for s != -1 {
		s, e, err = findBrackets(c, start)
		if err != nil {
			return parts, err
		}
		if s > start {
			parts = append(parts, c[start:s])
		}
		if s != -1 {
			parts = append(parts, c[s:e])
			start = e
		}
	}
	if start != len(c) {
		parts = append(parts, c[start:])
	}
	return parts, nil
}

func processText(t string) (Node, error) {
	return &Text{
		text: t,
	}, nil
}

func processDecorator(d string, v string) (Node, error) {
	return &Text{
		text: decorators[d](v),
	}, nil
}

func processStyle(s string, v string) (Node, error) {
	l, err := parse(v)
	if err != nil {
		return nil, err
	}

	l.style = styles[s]
	return l, nil
}

func processSource(s string) (Node, error) {
	return &Source{
		source: s,
	}, nil
}

func processMatrix(m string) (Node, error) {
	matrix := &Matrix{}

	rows, err := extractBrackets(m[1 : len(m)-1])
	if err != nil {
		return nil, err
	}

	for i, row := range rows {
		if i%2 == 1 {
			if strings.Trim(row, " \t") != "," {
				return nil, fmt.Errorf("matrix cols must be comma separated")
			}
		} else {
			nextRow := []Node{}
			parts := splitAtUnescaped(row[1:len(row)-1], '|', '\\')
			for _, part := range parts {
				ns, err := parse(part)
				if err != nil {
					return nil, err
				}
				nextRow = append(nextRow, ns)
			}
			matrix.elements = append(matrix.elements, nextRow)
		}
	}

	return matrix, nil
}

func processStatement(t string) (Node, error) {
	for dec := range decorators {
		if len(t) >= len(dec)+3 {
			if t[:len(dec)+2] == "{"+dec+":" {
				return processDecorator(dec, t[len(dec)+2:len(t)-1])
			}
		}
	}
	for sty := range styles {
		if len(t) >= len(sty)+3 {
			if t[:len(sty)+2] == "{"+sty+":" {
				return processStyle(sty, t[len(sty)+2:len(t)-1])
			}
		}
	}
	return processSource(t[1 : len(t)-1])
}

func findBrackets(code string, s int) (int, int, error) {
	last := ' '
	start, end := -1, -1
	startBracket := '-'
	for i, c := range code[s:] {
		if (c == '{' || c == '[') && last != '\\' {
			start = i + s
			startBracket = c
			break
		}
		last = c
	}
	if start == -1 {
		return -1, -1, nil
	}
	last = ' '
	depth := 0
	for i, c := range code[start+1:] {
		if (c == '{' || c == '[') && last != '\\' {
			depth++
		}
		if (c == '}' || c == ']') && last != '\\' {
			if (startBracket == '{' && c == '}') || (startBracket == '[' && c == ']') {
				if depth == 0 {
					end = i + start + 1
					break
				}
			}
			depth--
		}
		last = c
	}
	if end == -1 {
		return -1, -1, fmt.Errorf("could not find matching bracket for %v at %v", string(startBracket), start)
	}
	return start, end + 1, nil
}

func splitAtUnescaped(s string, del rune, esc rune) []string {
	parts := []string{}
	escaped := false
	last := 0
	for i, r := range s {
		if !escaped && r == del {
			parts = append(parts, s[last:i])
			last = i + 1
		}
		escaped = r == esc
	}
	parts = append(parts, s[last:])
	return parts
}

func stripLast(s string, n int) string {
	i := len(s) - n
	if i < 0 {
		i = 0
	}
	return s[:i]
}
