package main

import (
	"testing"
)

type t struct {
	name     string
	source   string
	expected string
	err      string
}

var (
	lineTests = []t{
		{
			name:     "No empty space between adjacent statements",
			source:   "{a}{b}Text",
			expected: "<{a}, {b}, 'Text'>",
		}, {
			source:   "{vec:v} {wht:=} [[{bvec.x2}],[{bvec.y2}]]",
			expected: "<'v⃗', ' ', <wht:'='>, ' ', mat[col[<{bvec.x2}>], col[<{bvec.y2}>]]>",
		}, {
			source:   "Transformed {vec:v} {wht:=} {red:{bvec.x2}{hat:i}} {wht:+} {bvec.y2}{hat:j}",
			expected: "<'Transformed ', 'v⃗', ' ', <wht:'='>, ' ', <red:{bvec.x2}, 'î'>, ' ', <wht:'+'>, ' ', {bvec.y2}, 'ĵ'>",
		}, {
			name:     "Trailing and leading text",
			source:   "AAA{foo}BBB",
			expected: "<'AAA', {foo}, 'BBB'>",
		}, {
			name:     "Trailing and leading statements",
			source:   "{foo}AAA{bar}",
			expected: "<{foo}, 'AAA', {bar}>",
		}, {
			name:     "Matrix",
			source:   "[[a|b], [c|d]]",
			expected: "<mat[col[<'a'>, <'b'>], col[<'c'>, <'d'>]]>",
		}, {
			name:   "Matrix, missing commas",
			source: "[[a|b] [c|d]]",
			err:    "matrix cols must be comma separated",
		}, {
			name:   "Matrix, incomplete",
			source: "[c|d]]",
			err:    "matrix must start with [[",
		}, {
			name:   "Matrix, incomplete",
			source: "[[c|d] foo]",
			err:    "matrix must end with ]]",
		}, {
			name:   "Matrix with no rows",
			source: "[]",
			err:    "minimal matrix must at least be [[]]",
		}, {
			name:   "Missing closing bracket",
			source: "AAA{BBB",
			err:    "could not find matching bracket for { at 3",
		},
	}
)

func TestLines(t *testing.T) {
	for _, test := range lineTests {
		t.Run(test.name, func(t *testing.T) {
			r, err := parse(test.source)
			errStr := ""
			if err != nil {
				errStr = err.Error()
			}
			if errStr != test.err {
				t.Errorf("\nsource       : %v\nexpected err : %v\nactual err   : %v", test.source, test.err, errStr)
			}
			if err != nil {
				return
			}
			if r.String() != test.expected {
				t.Errorf("\nsource   : %v\nexpected : %v\nactual   : %v", test.source, test.expected, r.String())
			}
		})
	}
}

func TestStripLast(t *testing.T) {
	test := func(i string, n int, o string) {
		t.Run(i, func(t *testing.T) {
			r := stripLast(i, n)
			if r != o {
				t.Errorf("stripLast(\"%v\", %v)\nexpected : %v actual   : %v", i, n, o, r)
			}
		})
	}

	test("a, b, c, ", 2, "a, b, c")
	test("a,b,c,", 1, "a,b,c")
	test("....", 4, "")
	test(".", 5, "")
}
