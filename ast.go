package main

type Visitor interface {
	List(*List) (string, error)
	Text(*Text) (string, error)
	Matrix(*Matrix) (string, error)
	Source(*Source) (string, error)
}

type Node interface {
	String() string
	Accept(Visitor) (string, error)
}

type List struct {
	elements []Node
	style    string
}

func (l *List) String() string {
	res := ""

	for _, n := range l.elements {
		res += n.String()
		res += ", "
	}

	st := ""
	if l.style != "" {
		st = l.style + ":"
	}

	return "<" + st + res[:len(res)-2] + ">"
}

func (l *List) Accept(v Visitor) (string, error) {
	return v.List(l)
}

type Text struct {
	text string
}

func (t *Text) String() string {
	return "'" + t.text + "'"
}

func (t *Text) Accept(v Visitor) (string, error) {
	return v.Text(t)
}

// Matrix is the ast node of a matrix element
type Matrix struct {
	elements [][]Node
}

// Width returns the width of the widest row in the matrix
func (m *Matrix) Width() int {
	max := 0
	for _, r := range m.elements {
		if len(r) > max {
			max = len(r)
		}
	}
	return max
}

// String returns a string representation of this matrix ast node
func (m *Matrix) String() string {
	mat := ""
	for _, r := range m.elements {
		cls := ""
		for _, c := range r {
			cls += c.String() + ", "
		}
		mat += "col[" + stripLast(cls, 2) + "], "
	}
	return "mat[" + stripLast(mat, 2) + "]"
}

// Accept will apply the provided visitor
func (m *Matrix) Accept(v Visitor) (string, error) {
	return v.Matrix(m)
}

// Source is a source code ast node
type Source struct {
	source string
}

// String will return a string representation of the ast source
func (s *Source) String() string {
	return "{" + s.source + "}"
}

// Accept will apply the provided visitor
func (s *Source) Accept(v Visitor) (string, error) {
	return v.Source(s)
}
