package main

import (
	"fmt"
	"testing"
)

type ct struct {
	name     string
	source   string
	expected string
}

var cts = []ct{
	{
		name:     "foo",
		source:   "I'm a yellow leaf",
		expected: "",
	},
	{
		name:     "bar",
		source:   "I'm a {yel:yellow} leaf",
		expected: "",
	},
}

func TestCoffee(t *testing.T) {
	for _, test := range cts {
		t.Run(test.name, func(t *testing.T) {
			ml := NewMathl()
			ml.parse(test.source, test.name)

			fmt.Println(ml.Source())
		})
	}
}
