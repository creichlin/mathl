package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

func main() {
	bytes, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		panic(err)
	}

	i := &input{}
	err = yaml.Unmarshal(bytes, i)
	if err != nil {
		panic(err)
	}

	mathl := NewMathl()
	for name, source := range i.Mathl {
		mathl.parse(source, name)
	}
	fmt.Println("window.mathl =")
	fmt.Println(mathl.Source())
	fmt.Println("    _m: {}")
	fmt.Println("    _update: (m)->")
	fmt.Println(mathl.UpdateSource())
}

type input struct {
	Mathl map[string]string `yaml:"mathl"`
}
