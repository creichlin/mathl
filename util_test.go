package main

import (
	"strconv"
	"testing"
)

func TestAsIdentifier(t *testing.T) {
	run := func(i int, exp string) {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			r := asIdentifier(i)
			if r != exp {
				t.Errorf("expected: %v\nactual: %v", exp, r)
			}
		})
	}

	run(0, "a")
	run(1, "b")
	run(24, "y")
	run(25, "ba")
	run(49, "bz") // a reserved word
}

func TestAsIdentifierRange(t *testing.T) {
	max := 30000
	res := map[string]bool{}
	for i := 0; i < 30000; i++ {
		res[asIdentifier(i)] = true
	}
	if len(res) != max {
		t.Errorf("some keys are not unique")
	}
}
