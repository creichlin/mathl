package main

type Mathl struct {
	coffee *CoffeeVisitor
	code   string
}

func NewMathl() *Mathl {
	return &Mathl{
		coffee: &CoffeeVisitor{
			indent: 1,
			node:   []string{"_r"},
		},
	}
}

func (m *Mathl) parse(source string, name string) error {
	ast, err := parse(source)
	if err != nil {
		return err
	}

	err = m.coffee.parse(ast, name)
	return err
}

func (m *Mathl) Source() string {
	return m.coffee.source
}

func (m *Mathl) UpdateSource() string {
	return m.coffee.updaters
}
