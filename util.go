package main

import (
	"strings"
)

// idChars are the available letters that can be used for identifiers
const idChars = "abcdefghijklmnopqrstuvwxy"

// reserved are reserved keywords that must not be used as identifier
// they will be mapped to another identifier using a char not in idChars (z)
var reserved = map[string]string{
	"by": "bz",
	"do": "dz",
}

func asIdentifier(i int) string {
	if i == 0 {
		return idChars[0:1]
	}
	r := ""
	for i > 0 {
		nv := i % len(idChars)
		r = string(idChars[nv]) + r
		i /= len(idChars)
	}
	if v, ok := reserved[r]; ok {
		// if it's a rserved word, return that keys value
		return v
	}
	return r
}

func asJsStr(i string) string {
	o := strings.Replace(i, "'", "\\'", -1)
	return "'" + o + "'"
}
